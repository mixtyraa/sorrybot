import { Localities, Segments } from './Edadeal';

export interface IRequestOffets {
  count?: number;
  locality: Localities;
  page?: number;
  segment?: Segments;
  q?: string;
}

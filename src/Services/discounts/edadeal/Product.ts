import { Field, Message, Type } from 'protobufjs/light';
import { storeList } from './Stores';

@Type.d('Product')
export default  class Product extends Message<Product> {
  @Field.d(2, 'string')
  public name: string;
  @Field.d(3, 'string')
  public img: string;
  @Field.d(4, 'float')
  public originCost: number;
  @Field.d(5, 'float')
  public discountCost: number;
  @Field.d(8, 'float')
  public volume: number;
  @Field.d(15, 'string')
  public from: string;
  @Field.d(16, 'string')
  public to: string;
  @Field.d(21, 'bytes')
  public companyByte: Buffer;

  public get company() {
    const id = this.companyByte.toString('base64');
    return storeList.find((value) => value.id === id);
  }
}

import { Field, Message, Type } from 'protobufjs/light';
import Product from './Product';

@Type.d('Message')
export default class Offers extends Message<Offers> {
  @Field.d(1, Product, 'repeated')
  public items: Product[];
}

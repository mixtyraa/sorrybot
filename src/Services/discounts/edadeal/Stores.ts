interface IStore {
  id: string;
  id2: string;
  name: string;
  code: string;
}

export const storeList: IStore[] = [
  {
    id: 'lNmHuELDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Магнит',
    code: 'magnit-univer'
  },
  {
    id: 'lNmDgkLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Пятёрочка',
    code: '5ka'
  },
  {
    id: 'lNmKm0LDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Монетка',
    code: 'monetka-super'
  },
  {
    id: 'lNmVKULDEeaUGVJUABC2CA==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Магнит Косметик',
    code: 'magnit-cosmo'
  },
  {
    id: 'lNmuQ0LDEeaUGVJUABC2CA==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'Бристоль',
    code: 'bristol'
  },
  {
    id: 'TYbRzng7Q+CjmiabplvTtg==',
    id2: 'aNUWslzOThCuXwwsByd9vQ==',
    name: 'Ozon',
    code: 'ozon'
  },
  {
    id: 'gSS03BCES4+/+2DDZTC98g==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'ОПТИМА',
    code: 'optima-ekb'
  },
  {
    id: 'rhXJFhmITNuq/PbkZMPo/w==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'Пив&Ко',
    code: 'pivko66'
  },
  {
    id: 'XCr11Q9OTkmJiTOnWedUiQ==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Парфюм-Лидер',
    code: 'parfum-lider'
  },
  {
    id: '+nZV6WjzSqyGNmQkzOYSIw==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Чебаркульская птица',
    code: 'chpt'
  },
  {
    id: 'lNmF3kLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Дикси',
    code: 'dixy'
  },
  {
    id: 'lNmEF0LDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Перекрёсток',
    code: 'perekrestok'
  },
  {
    id: 'HYVrw9oSRz2ARyaXPI9CiQ==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Тамара',
    code: 'tamara'
  },
  {
    id: 'yBC7la9sSh+0W5D6iXQYgQ==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Метрополис',
    code: 'metropolis-kgn'
  },
  {
    id: 'A8HSD8GcQFW7dJHBLXHZUA==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'Очаково',
    code: 'ochakovo'
  },
  {
    id: 'lNmNGkLDEeaUGVJUABC2CA==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'Детский мир',
    code: 'detmir'
  },
  {
    id: 'lNmlpELDEeaUGVJUABC2CA==',
    id2: 'GVfhLCnjSEixocPB6A7zMg==',
    name: 'Галамарт',
    code: 'galamart'
  },
  {
    id: 'lNmj1kLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Мегамарт',
    code: 'megamart'
  },
  {
    id: 'lNmIE0LDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Лента гипермаркет',
    code: 'lenta-giper'
  },
  {
    id: 'PgZZGPe9SE+g/cGAfSmllg==',
    id2: 'pfBUsEHWT6SXVPekKZuEaA==',
    name: 'Ле’Муррр',
    code: 'lemurrr'
  },
  {
    id: 's5zVm2yZRc2fGpJGM4k0YA==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'PampersOK',
    code: 'pampersok-tumen'
  },
  {
    id: 'lNnDfULDEeaUGVJUABC2CA==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'БЕРЕГИТЕ ЧУДО',
    code: 'beregitechudo'
  },
  {
    id: 'lNnD0ULDEeaUGVJUABC2CA==',
    id2: 'pfBUsEHWT6SXVPekKZuEaA==',
    name: 'Заповедник',
    code: 'zapovednik96'
  },
  {
    id: 'On3g4eoeQbOxufaD7kyNqA==',
    id2: 'GVfhLCnjSEixocPB6A7zMg==',
    name: 'Home Market',
    code: 'home_market'
  },
  {
    id: 'lNmIZELDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'О\'КЕЙ гипермаркет',
    code: 'okmarket-giper'
  },
  {
    id: 'lNmN/kLDEeaUGVJUABC2CA==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'Дочки-Сыночки',
    code: 'dochkisinochki'
  },
  {
    id: 'lNmOSkLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Магнит Семейный',
    code: 'magnit-giper'
  },
  {
    id: 'lNmeR0LDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Фасоль',
    code: 'myfasol'
  },
  {
    id: 'lNmqJELDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Райт',
    code: 'raitfresh'
  },
  {
    id: 'fRprzTgASyW+FKDahOdsng==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'AZUMA',
    code: 'azuma'
  },
  {
    id: 'lNmt9ELDEeaUGVJUABC2CA==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Рив Гош',
    code: 'rivegauche'
  },
  {
    id: 'hnohhEK/SBqMlCYWVKssvA==',
    id2: 'GVfhLCnjSEixocPB6A7zMg==',
    name: 'Мир удивительных товаров',
    code: 'mir_udivitelnykh_tovarov'
  },
  {
    id: 'GhmrCNg3SwKUupw5nBF5JA==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Marafett',
    code: 'marafett'
  },
  {
    id: 'lNmJ+ELDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'METRO',
    code: 'metro-cc'
  },
  {
    id: 'lNmJpELDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Ашан',
    code: 'auchan'
  },
  {
    id: 'eu8nyOSvTSm8lxRuB9nknw==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'Kari Kids',
    code: 'kari_kids'
  },
  {
    id: 'lNmFLkLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Карусель',
    code: 'karusel'
  },
  {
    id: 'sV3An5cJR2+zTJKT8czxSg==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'Мильстрим',
    code: 'milstrim'
  },
  {
    id: 't6OPCl3XTAme8QLTXgVFzQ==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Белорусская косметика',
    code: 'bykosmetika'
  },
  {
    id: 'Hw4vEytPT9qpeqQUXXJWSg==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Низкоцен',
    code: 'nizkocen'
  },
  {
    id: 'AxP2jsVoS5iCsAsPzmskJg==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'Разливной',
    code: 'razlivnoj'
  },
  {
    id: 'lNnIR0LDEeaUGVJUABC2CA==',
    id2: 'pfBUsEHWT6SXVPekKZuEaA==',
    name: 'Четыре лапы',
    code: '4lapy'
  },
  {
    id: 'lNmlV0LDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Ашан сити',
    code: 'auchan-city'
  },
  {
    id: 'Il5xpM0uRX2aTgUjOi5IOA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Юбилейный',
    code: 'ishimskii_mk'
  },
  {
    id: '1xFUSMM6SK+1nGdRwcJhLg==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Niagara',
    code: 'niagara'
  },
  {
    id: 'UNWTiNorS1eyUBD2jik59Q==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Сибирские колбасы',
    code: 'sibirskie_kolbasy'
  },
  {
    id: 'LxBPbd7lQGuie29roImE8A==',
    id2: 'xNlDhuZURKWZIaNz5sYBrA==',
    name: 'Rich Family',
    code: 'rich_family'
  },
  {
    id: 'rRvOg+OHR2KOa4GAB5MJXg==',
    id2: 'MLhf+BKhTM+8MV8t5DHa0Q==',
    name: 'Lamel',
    code: 'lamel'
  },
  {
    id: 'lNm04kLDEeaUGVJUABC2CA==',
    id2: 'sN4J8EkWSQaPmnSCMVQYQg==',
    name: 'Красный Яр',
    code: 'krasyar'
  },
  {
    id: 'xZupNMsFTNKlb1h7qszdcw==',
    id2: '469ID6qOS02Dg2ODFcIs/g==',
    name: 'МАВТ-Винотека',
    code: 'mavt'
  },
];

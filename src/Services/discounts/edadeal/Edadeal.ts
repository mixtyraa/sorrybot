import Axios from 'axios';
import { IRequestOffets } from './interfaces';
import Offers from './Offers';

export enum Segments {
  'whiskey' = 'whiskey',
}

export enum Localities {
  'tumen' = 'tumen',
}

export default class Edadeal {
  protected url: string = 'https://api.edadeal.ru/web/search/offers';

  public async getOffers(data: IRequestOffets) {
    const response = (await Axios.get(this.url, {
      params: data,
      responseType: 'arraybuffer',
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
      }
    })).data;

    const offers = Offers.decode(response).items;

    return offers;
  }
}

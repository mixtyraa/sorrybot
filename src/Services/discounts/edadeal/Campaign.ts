import { Field, Message, Type } from 'protobufjs/light';
import Product from './Product';

@Type.d('Message')
export default class Compaign extends Message<Compaign> {
  @Field.d(1, Product, 'repeated')
  public items: Product[];
}

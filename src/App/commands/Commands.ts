import { MessageContext } from "vk-io";
import { Trello } from "~/Integration/trello/TrelloWrap";
import { VK } from "~/Integration/VkIntegration";
import { EventTypes } from "../Event";

import dateformat from 'dateformat';
import { getVersion } from "~/Helper/getVersion";
import Helper from "~/Helper/Helper";
import Premiercinema from "~/Services/cinemas/Premiercinema";
import Edadeal, { Localities, Segments } from "~/Services/discounts/edadeal/Edadeal";
import { IRole } from "../members/interfaces";
import { IMessageContextExtended } from "../VkBot/midleware/interfaces";
import { ListCommands } from "./ListCommands";

export async function start(ctx: MessageContext): Promise<string> {
  try {
    const vkId = ctx.peerId;
    const chatInfo = await VK.getConversations(vkId);
    await Trello.addChat(chatInfo[0].name, vkId.toString());
    return 'Веселимся, ребят!🍾🎉';
  } catch (e) {
    console.log(e);
    return 'Не удалось экспортировать чат';
  }
}

export async function getAcceptedEvents(ctx: MessageContext): Promise<string> {
  const events = await Trello.getEvents(EventTypes.ACCEPTED);
  let result: string = '';

  events.filter((event) => event.chats.find((chat) => +chat.vkId === ctx.peerId))
    .forEach((event, idx) => {
      result += `\n`;
      result += `${idx + 1}) ${event.name}\n`;
      if (event.description) {
        result += `${event.description}\n`;
      }

      if (event.dateStart.getTime() !== 0) {
        result += `Когда: ${dateformat(event.dateStart, 'dd.mm.yyyy HH:MM')}`;
      }
      result += `\n`;
  });

  if (result.length === 0) {
    result = 'У вас нет событий вы талые 👎👎👎💩🙉🙈';
  }

  return result;
}

export async function getAbout(ctx: MessageContext): Promise<string> {
  let ver = null;
  try {
    ver = await getVersion();
  } catch {
    ver = 'Тебя вообще ебёт какая ???!?!!?';
  }

  let result = 'Sorry Bot 🏴‍☠😎';
  if (ver) {
    result += `\nVersion: ${ver}`;
  }
  return result;
}

export async function getPosterToday(ctx: MessageContext): Promise<string> {
  const pc = new Premiercinema();
  const films = await pc.getPosterToday();
  let result = '';
  films.forEach((film, idx) => {
    result += `
${idx + 1}) ${film.name}
Страна ${film.country}
Жанр ${film.genre}
Продолжительность ${film.duration}
В кино ${film.startdate}

`;
  });
  return result;
}

export async function getHelp(ctx: IMessageContextExtended): Promise<string> {
  let result = 'Вам доступны следующие команды:\n';
  const allowedAction = [...new Set(Helper.flat(ctx.member.roles.map((role) => role.commands)))];

  ListCommands.forEach((commnad) => {
    if (!commnad.info) {
      return true;
    }

    let allowed = false;
    if (ctx.isChat) {
      allowed = commnad.canChat;
    } else {
      allowed = commnad.canUser;
    }

    if (allowed && (allowedAction.indexOf(commnad.code) !== -1 || allowedAction.indexOf('*') !== -1)) {
      result += `${commnad.commnad} -- ${commnad.info}\n`;
    }
  });

  return result;
}

export async function tea(ctx: IMessageContextExtended): Promise<string> {
  const answer = [
    'Кристя, ну какой ещё чай',
    'неа',
    'в пизду',
    'сама ставь',
    'рыся в чайник насала',
    'я тебе что раб',
    'ага',
    'сейчас очередь рыси',
    'угу',
    'ajhahah',
  ];
  return answer[Helper.randomNumber(0, answer.length - 1)];
}

export async function getDiscounts(ctx: IMessageContextExtended): Promise<string> {
  const edadeal = new Edadeal();
  if (!ctx.stringParams) {
    return `Ебать не должно.
Ещё блять про скидки на тампоны спроси`;
  }
  const offers = await edadeal.getOffers({
    count: 10,
    locality: Localities.tumen,
    q: ctx.stringParams,
    page: 1,
  });
  if (offers.length === 0) {
    return 'Сегодня не бухаем';
  }
  let result = '';
  offers.forEach((product, idx) => {
    const nameCompany = product.company ? product.company.name : 'не ебу где, базу обнови бля';
    result += `
${idx + 1}) ${product.name}
Скидка: ${Math.round(product.discountCost)} ₽ (${Math.round(product.originCost)} ₽)
Где: ${nameCompany}
Успей до ${dateformat(product.to, 'dd.mm')}

`;
  });
  return result;
}

export async function getAlcohelp(ctx: IMessageContextExtended): Promise<string> {
  const db = [
    {
      name: 'Текила',
      advice: [
        {
          groups: ['DIGESTION'],
          content: 'улучшает пищеварение',
        },
        {
          groups: [],
          content: 'предотвращает деменцию',
        },
        {
          groups: ['BONES'],
          content: 'способствует здоровью костей',
        },
        {
          groups: ['LOSINGWEIGHT'],
          content: 'не провоцирует переедание',
        },
        {
          groups: ['PAINKILLER'],
          content: 'действует как болеутоляющее',
        },
        {
          groups: ['PAINKILLER'],
          content: 'снижает болевой порого чувствительности',
        },
        {
          groups: ['HEART'],
          content: 'расширяет кровеносные сосуды',
        },
        {
          groups: ['DEMENTIA'],
          content: 'снижает шансы развития слабоумия',
        },
      ]
    },
    {
      name: 'Джин',
      advice: [{
          groups: ['TENSION'],
          content: 'снимает нервое напряжение',
        },
        {
          groups: ['SADNESS', 'INSOMNIA'],
          content: 'избавляет от депресии и бессонницы',
        },
        {
          groups: ['SADNESS'],
          content: 'поднимает настроение',
        },
        {
          groups: [],
          content: 'снимает боль в пояснице',
        },
        {
          groups: ['REDNESS'],
          content: 'помогает от припухлостей и покраснений',
        },
      ]
    },
    {
      name: 'Водка',
      advice: [{
          groups: ['INSOMNIA'],
          content: 'нормализирует сон',
        },
        {
          groups: ['COLD'],
          content: 'помогает при простуде',
        },
        {
          groups: ['APPETITE'],
          content: 'улучшает аппетит',
        },
        {
          groups: ['IMMUNITY'],
          content: 'тонизирует иммунную систему',
        },
        {
          groups: ['COUGH'],
          content: 'помогает избавиться от приступов кашля',
        },
        {
          groups: ['HEADACHE'],
          content: 'избавляет от головной боли',
        },
      ]
    },
    {
      name: 'Настройка',
      advice: [
        {
          groups: ['HEART'],
          content: 'улучшает работу сердечно-сосудистой системы',
        },
        {
          groups: ['DIGESTION'],
          content: 'выводит вредные вещества из организма',
        },
        {
          groups: ['DIGESTION'],
          content: 'помогает снизить уровень холестерина в крови',
        },
        {
          groups: [],
          content: 'действиет, как антисептическое средство',
        },
        {
          groups: ['COLD'],
          content: 'эффективна при простуде',
        },
      ]
    },
    {
      name: 'Бренди',
      advice: [{
          groups: ['COLD', 'COUGH', 'THROAT'],
          content: 'помогает при кашле или боле в горле',
        },
        {
          groups: ['OLDAGE'],
          content: 'предотвращает появление морщин',
        },
        {
          groups: ['OLDAGE', 'VISION'],
          content: 'улучшает зрение',
        },
        {
          groups: ['FATIGUE'],
          content: 'расслабляет организм',
        },
        {
          groups: ['INSOMNIA', 'SLEEPINGMODE'],
          content: 'нормализирует сон бессоница',
        },
        {
          groups: ['IMMUNITY'],
          content: 'стимулирует работу иммунной системы',
        },
      ]
    },
    {
      name: 'Абсент',
      advice: [{
          groups: ['DIGESTION'],
          content: 'стимулирует выработку пищеварительного сока',
        },
        {
          groups: ['APPETITE'],
          content: 'улучшает аппетит',
        },
        {
          groups: ['OLDAGE'],
          content: 'помогает при болях в суставах',
        },
        {
          groups: ['COLD', 'TEMPERATURE'],
          content: 'помогает при высокой температуре',
        },
      ]
    },
    {
      name: 'Виски',
      advice: [
        {
          groups: ['LOSINGWEIGHT'],
          content: 'помогает избежать лишнего веса',
        },
        {
          groups: ['HEART'],
          content: 'улучшает работу сердца',
        },
        {
          groups: [],
          content: 'помогает побороть раковые заболевания',
        },
        {
          groups: ['DEMENTIA'],
          content: 'улучшает работу мозга',
        },
        {
          groups: ['TENSION', 'STRESS'],
          content: 'помогает побороть стресс',
        },
        {
          groups: ['DIGESTION'],
          content: 'улучшает работу кишечника',
        },
      ]
    },
    {
      name: 'Вино',
      advice: [{
          groups: ['TENSION', 'STRESS'],
          content: 'природный антидепрессант',
        },
        {
          groups: ['COLD'],
          content: 'противовоспалительное вещество',
        },
        {
          groups: [],
          content: 'уничтожает бактерии туберкулеза и холеры',
        },
        {
          groups: ['OLDAGE'],
          content: 'продлевает жизнь',
        },
        {
          groups: ['DIGESTION'],
          content: 'нормализирует обмен веществ',
        },
        {
          groups: ['SPORT'],
          content: 'способствует умственной и физической деятельности',
        },
      ]
    }
  ];

  const group = ctx.action.fulfillmentText;
  const alcoList = [];
  db.forEach((alco) => {
    const listDvice = alco.advice.filter((adv) => {
      return adv.groups.indexOf(group) !== -1;
    });
    if (listDvice.length > 0) {
      alcoList.push({
        name: alco.name,
        advice: listDvice,
      });
    }
  });

  let result = '';
  alcoList.forEach((alco) => {
    result += `${alco.name}:
`;
    alco.advice.forEach((adv) => {
      result += `- ${adv.content}
`;
    });
  });
  return result;
}

import Member from '~/App/members/Member';
import { IMessageContextExtended } from './interfaces';

/**
 * Идентифицирует пользователя в системе
 *
 * @param ctx
 * @param next
 */
export async function loadMember(ctx: IMessageContextExtended, next: () => {}) {
  ctx.member = await Member.getUser(`id${ctx.senderId}`);
  next();
}

import { Dialogflow } from "~/Integration/dialogflow/DialogflowWrap";
import { IMessageContextExtended } from "./interfaces";

export async function dialogflow(ctx: IMessageContextExtended, next: () => {}) {
  ctx.action = await Dialogflow.defineAction(ctx.text);

  if (ctx.action.action) {
    next();
  }
}

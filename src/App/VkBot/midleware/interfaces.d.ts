import {MessageContext} from 'vk-io';
import { ICommand } from '~/App/commands/interfaces';
import Member from '~/App/members/Member';
import { IAction } from '~/Integration/dialogflow/interfaces';

export interface IMessageContextExtended extends MessageContext {
  isCommand: boolean;
  member: Member;
  action: IAction;
  command: ICommand;
  stringParams: string;
}

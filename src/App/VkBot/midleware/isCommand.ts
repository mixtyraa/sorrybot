import Executor from '../Executor';
import { command } from './command';
import { IMessageContextExtended } from './interfaces';

/**
 * Устанавливает признак команды,
 * заверщаем работу обработки запроса
 *
 * @param ctx
 * @param next
 */
export function isCommand(ctx: IMessageContextExtended, next: () => void) {
  ctx.isCommand = (ctx.text || '')[0] === '/';

  if (ctx.isCommand) {
    command(ctx, () => {
      Executor.do(ctx);
    });
  } else {
    next();
  }
}

export interface IAction {
  action: string;
  params: {[key: string]: any; };
  fulfillmentText: string;
}


import dialogflow from 'dialogflow';
import fs from 'fs';
import path from 'path';
import uuid from 'uuid';
import { IAction } from './interfaces';

class DialogflowWrap {
  private privateKey = '';

  public constructor() {
    this.privateKey = fs.readFileSync(path.resolve(process.env.DIALOGFLOW_PRIVATE_KEY), 'utf8');
  }

  public async defineAction(text: string): Promise<IAction> {
    const sessionId = uuid.v4();
    const sessionClient = new dialogflow.SessionsClient({
      credentials: {
        client_email: process.env.DIALOGFLOW_CLIENT_EMAIL,
        private_key: this.privateKey,
      }
    });
    const sessionPath = sessionClient.sessionPath(process.env.DIALOGFLOW_PROJECT_ID, sessionId);
    const request: dialogflow.DetectIntentRequest = {
      session: sessionPath,
      queryInput: {
        text: {
          text,
          languageCode: 'ru',
        },
      },
    };

    const response = await sessionClient.detectIntent(request);

    const params = {};
    if (typeof response[0].queryResult.parameters.fields === 'object') {
      // сомнительная хуйня, но пока так , нужно вникнуть в тему
      Object.keys(response[0].queryResult.parameters.fields).forEach((key) => {
        params[key] = response[0].queryResult.parameters.fields[key].stringValue;
      });
    }

    const fulfillmentText = response[0].queryResult.fulfillmentText;
    return {
      action: response[0].queryResult.action,
      params,
      fulfillmentText
    };

  }

}

export const Dialogflow = new DialogflowWrap();

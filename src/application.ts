import bugsnag from '@bugsnag/js';
import Bot from '~/App/VkBot/Bot';
import { Trello } from './Integration/trello/TrelloWrap';

console.log('Environment:', process.env.NODE_ENV);

(async () => {
  bugsnag({
    apiKey: process.env.BUGSNAG_KEY,
    notifyReleaseStages: [ 'production' ],
  });

  await Trello.start().then(() => {
    console.log('Trello init');
  }).catch((d) => console.error(d.request));

  new Bot().start().then(() => {
    console.log('Valera is stasdrted');
  }).catch(console.error);
})();

FROM node:12-alpine AS prod
WORKDIR /home/node/sorrybot
COPY . .
RUN apk add --no-cache git
RUN yarn install
CMD yarn start

FROM node:12-alpine AS dev
WORKDIR /home/node/sorrybot
RUN apk add --no-cache git
CMD yarn start:dev
